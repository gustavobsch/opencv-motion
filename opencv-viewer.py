#!/usr/bin/python3
import configparser, logs, os, imutils, cv2, base64, json
import numpy as np
from flask import Flask, Response, render_template
from kafka import KafkaConsumer
from utils import utils as utils

logger = logs.setup_logger('opencv-motion')
logger.warning("Program Started...")

app = Flask(__name__)

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/cameras/<topic>')
def gen(topic):
	# return a multipart response
	return Response(kafkastream(topic),
					mimetype='multipart/x-mixed-replace; boundary=frame')

def string2np(string):
	frame = cv2.imdecode(np.fromstring(string2base64(string) , np.uint8), cv2.IMREAD_COLOR)
	return frame

def string2bytes(string):
	jpeg = cv2.imencode('.jpg', string2np(string))[1].tobytes()
	return jpeg

def string2base64(string):
	frame = base64.b64decode(string)
	return frame

def kafkastream(topic):
	#connect to Kafka server and pass the topic we want to consume
	consumer = KafkaConsumer(topic, group_id='view', bootstrap_servers = config['kafka_broker_hostname']+":"+str(config['kafka_broker_port']))
	#Continuously listen to the connection and print messages as recieved
	for msg in consumer:
		#msg.value = resize(msg.value)
		frame_metadata = json.loads(msg.value.decode('utf-8'))
		image = string2bytes(frame_metadata['frame'])
			
		yield (b'--frame\r\n'
			   b'Content-Type: image/jpg\r\n\r\n' + image + b'\r\n\r\n')


	
def label(image):
	## decode and resize
	nparr = np.fromstring(image, np.uint8)
	#frame_resized = cv2.resize(cv2.imdecode(nparr, cv2.IMREAD_COLOR), (629, 470))
	image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
	# add time stamp
	#frame_resized = cv2.rectangle(frame_resized, (0, frame_resized.shape[0]), (frame_resized.shape[1], frame_resized.shape[0] - 16), (0,0,0), cv2.FILLED)
	#cv2.putText(frame_resized, ts, (frame_resized.shape[0] - 60, frame_resized.shape[0] - 3), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
	#cv2.putText(frame_resized, self.deviceid, (2, self.frame_resized.shape[0] - 3), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
	#self.frame = cv2.line(self.frame, (0, self.frame.shape[0] - 10, self.frame.shape[0]- 0), (self.frame.shape[:2]), (0, 255, 0),6)
	#self.frame = cv2.rectangle(self.frame, (0, self.frame.shape[0] - 10), (self.frame.shape[0], 0), (0,255,0), 5)

	image = cv2.imencode('.jpg', image)[1].tobytes()
	return image

def config_load():
	global config
	config = {}
	config_file = configparser.RawConfigParser()
	config_file.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), '', 'config.cfg'))
	config.update({
	'kafka_broker_hostname':utils.chop_comment(config_file.get('kafka_broker', 'hostname')),
	'kafka_broker_port':int(utils.chop_comment(config_file.get('kafka_broker', 'port'))),
	'kafka_broker_compression':utils.chop_comment(config_file.get('kafka_broker', 'compression')),
	'flask_ip':utils.chop_comment(config_file.get('flask', 'bind_address')),
	'flask_port':int(utils.chop_comment(config_file.get('flask', 'port'))),
	'flask_debug':config_file.getboolean('flask', 'debug'),
	})

if __name__ == '__main__':
	logger.warning("Loading configuration parameters from config file.")
	config_load()
	# Run flask here
	app.run(host=config['flask_ip'], port = config['flask_port'], debug = config['flask_debug'], threaded=True)


# decode with decimg = cv2.imdecode(encimg, 1)

