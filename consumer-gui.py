import time
import sys
from kafka import KafkaConsumer
import numpy as np
import cv2
import argparse
import json
import base64

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-t", "--topic",
	help="Kafka Topic")
ap.add_argument("-v", "--video",
	help="Video to play")
ap.add_argument("-s", "--save",
	help="Save Video")
args = vars(ap.parse_args())

if(args["save"]):
	print("Save Output Enabled! %s" % args["save"])
	#fourcc = cv2.VideoWriter_fourcc(*'mp4v')
	fourcc = cv2.VideoWriter_fourcc('D','I','V','X')
	out = cv2.VideoWriter(args["save"], fourcc, 20.0, (800, 600))
	
count = 0
fps = 0
starttime = time.time()
kbps = 0

def string2np(string):
	frame = cv2.imdecode(np.fromstring(string2base64(string) , np.uint8), cv2.IMREAD_COLOR)
	return frame

def string2base64(string):
	frame = base64.b64decode(string)
	return frame
	
if args["video"]:
	#vs = VideoStream(usePiCamera=True).start()
	vs = cv2.VideoCapture(args["video"])
	#vs = VideoStream(src=0).start()
	# loop over the frames from the video stream
	# save locally if enabled
	while True:
		count += 1
		# grab the frame from the threaded video stream and resize it
		# to have a maximum width of 400 pixels
		image = vs.read()[1]
		image = cv2.rectangle(image, (0, image.shape[0]), (image.shape[1], image.shape[0] - 16), (0,0,0), cv2.FILLED)
		cv2.putText(image, "FPS: "+str(fps), (2, image.shape[0] - 3), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
		# save frame locally 
		if(args["save"]):
			out.write(image)
			
		# show the output frame
		cv2.imshow("Video", image)
		key = cv2.waitKey(1) & 0xFF
		if count == 5:
			fps = round((count / ((time.time() - starttime))), 2)
			print("FPS: %s KBPS: %s" % (fps, kbps))
			starttime = time.time()
			count = 0
		# if the `q` key was pressed, break from the loop
		if key == ord("q"):
			break
		#time.sleep(0.001)
	vs.stop()

elif args["topic"]:
	consumer = KafkaConsumer(args["topic"], bootstrap_servers=['kafka.noip.us:9093'])
	while True:
		for msg in consumer:
			count += 1
			frame_metadata = json.loads(msg.value.decode('utf-8'))
			image = string2np(frame_metadata['frame'])
			camera = frame_metadata['camera']
			lag = round(time.time() - frame_metadata['lag'], 3)
			kbps = int(sys.getsizeof(image)/1024)
			image = cv2.rectangle(image, (0, image.shape[0]), (image.shape[1], image.shape[0] - 16), (0,0,0), cv2.FILLED)
			cv2.putText(image, "FPS: "+str(fps), (2, image.shape[0] - 3), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
			#cv2.putText(self.frame_original, ts, (self.frame_original.shape[0] - 60, self.frame_original.shape[0] - 3), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
			# save frame locally 
			if(args["save"]):
				out.write(image)
			# show the output frame
			cv2.imshow("Frame", image)
			key = cv2.waitKey(1) & 0xFF
			# if the `q` key was pressed, break from the loop
			if key == ord("q"):
				break
			if count == 5:
				fps = round((count / ((time.time() - starttime))), 2)
				print("Camera: %s FPS: %s KBPS: %s LAG: %sms" % (camera, fps, kbps, lag))
				starttime = time.time()
				count = 0


	
# do a bit of cleanup
cv2.destroyAllWindows()
if(args["save"]):
	out.release()
