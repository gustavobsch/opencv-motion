#!/usr/bin/python3
import configparser, os, utils, datetime, time, logs, socket, threading, sys, cv2, json, numpy, base64, pytz, imutils, heapq
from queue import Queue
from queue import PriorityQueue
import paho.mqtt.client as mqtt
from kafka import KafkaProducer, KafkaConsumer
from datetime import datetime
from threading import Thread
from utils import utils
import numpy as np, math
from openvino.inference_engine import IENetwork, IEPlugin
import multiprocessing as mp

logger = logs.setup_logger('opencv-openvino')
logger.warning("Program Started...")
starttime = time.time()

### Get run mode
if len(sys.argv) > 1:
	runmode = sys.argv[1]
elif len(sys.argv) == 1:
	runmode = 'normal'

env = os.environ.copy()

classifier = env.get('classifier', True)
camera_class = env.get('camera_class', True)

if env.get('DISPLAY', False):
	display = True
else:
	display = False


def config_load():
	global config
	global in_queue
	global out_queue
	global out_queue_frame
	global save_queue
	in_queue = priority_queue()
	out_queue = Queue()
	out_queue_frame = Queue()
	save_queue = Queue()
	config = {}
	config_file = configparser.RawConfigParser()
	config_file.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), '', 'config.cfg'))
	config.update({
	'timezone':config_file.get('environment', 'timezone'),
	'mqtt_broker_enabled':config_file.getboolean('mqtt_broker', 'enabled'),
	'mqtt_broker_hostname':utils.chop_comment(config_file.get('mqtt_broker', 'hostname')),
	'mqtt_broker_port':int(utils.chop_comment(config_file.get('mqtt_broker', 'port'))),
	'mqtt_broker_username':utils.chop_comment(config_file.get('mqtt_broker', 'username')),
	'mqtt_broker_pass':utils.chop_comment(config_file.get('mqtt_broker', 'password')),
	'mqtt_publish_topic':utils.chop_comment(config_file.get('mqtt_broker', 'ncs-publish_topic')),
	'mqtt_publish_format':utils.chop_comment(config_file.get('mqtt_broker', 'format')),
	'influxdb_enabled':config_file.getboolean('influxdb', 'enabled'),
	'influxdb_hostname':utils.chop_comment(config_file.get('influxdb', 'hostname')),
	'influxdb_port':int(utils.chop_comment(config_file.get('influxdb', 'port'))),
	'influxdb_username':utils.chop_comment(config_file.get('influxdb', 'username')),
	'influxdb_password':utils.chop_comment(config_file.get('influxdb', 'password')),
	'influxdb_dbname':utils.chop_comment(config_file.get('influxdb', 'database')),
	'webdavclient_enabled':config_file.getboolean('webdavclient', 'enabled'),
	'webdavclient_hostname':utils.chop_comment(config_file.get('webdavclient', 'hostname')),
	'webdavclient_port':int(utils.chop_comment(config_file.get('webdavclient', 'port'))),
	'webdavclient_username':utils.chop_comment(config_file.get('webdavclient', 'username')),
	'webdavclient_password':utils.chop_comment(config_file.get('webdavclient', 'password')),
	'webdavclient_folder':utils.chop_comment(config_file.get('webdavclient', 'folder')),
	'kafka_broker_hostname':utils.chop_comment(config_file.get('kafka_broker', 'hostname')),
	'kafka_broker_port':int(utils.chop_comment(config_file.get('kafka_broker', 'port'))),
	'kafka_broker_subscribe':utils.chop_comment(config_file.get('kafka_broker', 'subscribe')),
	'mobilenet_classifier_confidence':float(utils.chop_comment(config_file.get('mobilenet_ncs', 'confidence'))),
	'mobilenet_classifier_prototxt':os.path.join(os.path.abspath(os.path.dirname(__file__)), utils.chop_comment(config_file.get('mobilenet_ncs', 'prototxt'))),
	'mobilenet_classifier_weights':os.path.join(os.path.abspath(os.path.dirname(__file__)), utils.chop_comment(config_file.get('mobilenet_ncs', 'weights'))),
	'tiny_yolo_classifier_confidence':float(utils.chop_comment(config_file.get('tiny_yolo', 'confidence'))),
	'tiny_yolo_classifier_model_bin':os.path.join(os.path.abspath(os.path.dirname(__file__)), utils.chop_comment(config_file.get('tiny_yolo', 'model_bin'))),
	'tiny_yolo_classifier_model_xml':os.path.join(os.path.abspath(os.path.dirname(__file__)), utils.chop_comment(config_file.get('tiny_yolo', 'model_xml'))),
	'yolo_classifier_confidence':float(utils.chop_comment(config_file.get('yolo', 'confidence'))),
	'yolo_classifier_model_bin':os.path.join(os.path.abspath(os.path.dirname(__file__)), utils.chop_comment(config_file.get('yolo', 'model_bin'))),
	'yolo_classifier_model_xml':os.path.join(os.path.abspath(os.path.dirname(__file__)), utils.chop_comment(config_file.get('yolo', 'model_xml'))),
	})

class priority_queue(PriorityQueue):
	def __init__(self):
		PriorityQueue.__init__(self)
		self.counter = 0
	def put(self, priority, item):
		PriorityQueue.put(self, (priority, self.counter, item))
		self.counter += 1
	def get(self, *args, **kwargs):
		_, _, item = PriorityQueue.get(self, *args, **kwargs)
		return item

def camera_db_load():
	global cameras
	cameras = {}
	# Load camera_db config file
	camera_db = configparser.RawConfigParser()
	camera_db.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), '', 'camera_db.cfg'))
	# Fill camera_db dictionary
	for camera in camera_db.sections():
		cameras[camera] = {}
		for key, value in camera_db.items(camera):
			cameras[camera][key] = value

def check_network():
	mqtt = 0
	kafka = 0
	webdavclient = 0
	influxdb = 0
	while mqtt == 0:
		try:
			s = socket.create_connection((socket.gethostbyname(config['mqtt_broker_hostname']), config['mqtt_broker_port']), 2)
			logger.warning("MQTT broker network check passed!, '%s' is reachable." % config['mqtt_broker_hostname'])
			mqtt = 1
		except Exception:
			logger.warning("MQTT broker network check failed!, stalling until it becomes reachable...")
			time.sleep(15)
	while kafka == 0:
		try:
			s = socket.create_connection((socket.gethostbyname(config['kafka_broker_hostname']), config['kafka_broker_port']), 2)
			logger.warning("Kafka network check passed!, '%s' is reachable." % config['kafka_broker_hostname'])
			kafka = 1
		except Exception:
			logger.warning("Kafka network check failed!, stalling until it becomes reachable...")
			time.sleep(15)
	while webdavclient == 0:
		try:
			s = socket.create_connection((socket.gethostbyname(config['webdavclient_hostname']), config['webdavclient_port']), 2)
			logger.warning("Webdavclient network check passed!, '%s' is reachable." % config['webdavclient_hostname'])
			webdavclient = 1
		except Exception:
			logger.warning("Webdavclient network check failed!, stalling until it becomes reachable...")
			time.sleep(15)
	while influxdb == 0:
		try:
			s = socket.create_connection((socket.gethostbyname(config['influxdb_hostname']), config['influxdb_port']), 2)
			logger.warning("Influxdb network check passed!, '%s' is reachable." % config['influxdb_hostname'])
			influxdb = 1
		except Exception:
			logger.warning("Influxdb network check failed!, stalling until it becomes reachable...")
			time.sleep(15)

def get_threads():
	threads = []
	main_thread = threading.current_thread()
	for t in threading.enumerate():
		if t is main_thread:
			continue
			#print(t.getName())
		threads.append(t.getName())
	return threads

### MQTT funcionts here
def on_connect(client, userdata, flags, rc):
	# Subscribing in on_connect() means that if we lose the connection and
	# reconnect then subscriptions will be renewed.
	if rc == 0:
		logger.warning("Succesfully connected to MQTT broker, starting subscriptions if any.")
		# Start Subscriptions here
		#client.subscribe(config['mqtt_subscribe_wind_topic'], 1)
		#logger.warning("Subscribed to topic '%s'" % config['mqtt_subscribe_wind_topic'])
	else:
		logger.warning("Connection to MQTT broker failed. rc='%s' " % rc)

def on_publish(client, userdata, mid):
	logger.debug("PUBLISH '%s' completed." % mid)

def on_message(client, userdata, msg):
	global wind
	if msg.topic == config['mqtt_subscribe_wind_topic']:
		wind = float(msg.payload)
		if runmode == 'debug':
			print("Refreshing wind speed %s" % wind)

def on_disconnect(client, userdata, rc):
	if rc != 0:
		logger.warning("Unexpected MQTT Broker disconnection. Will auto-reconnect.")

def on_log(mosq, obj, level, string):
	logger.debug(string)

### Classes / functions start here
class mqtt_connect(threading.Thread):
	def __init__(self, name = None):
		threading.Thread.__init__(self, name = name)
		return
	def run(self):
		global mqttc
		mqttc = mqtt.Client()
		#mqttc.username_pw_set(config['mqtt_broker_pass'], config['mqtt_broker_pass'])
		mqttc.connect(config['mqtt_broker_hostname'], config['mqtt_broker_port'], 80)
		#mqttc.on_connect = on_connect
		#mqttc.on_message = on_message
		mqttc.on_publish = on_publish
		mqttc.on_disconnect = on_disconnect
		# Uncomment here to enable MQTT debug messages
		#mqttc.on_log = on_log
		while True:
			mqttc.loop()

def publish_mqtt(payload):
	### Add timestamp and delete image from payload
	now = datetime.now(pytz.timezone(config['timezone'])).strftime('%Y-%m-%d %H:%M:%S')
	payload.update({'ts':now})
	### Get publish format
	if config['mqtt_publish_format'] == 'json' or config['mqtt_publish_format'] == 'both':
		# Pass the container to the MQTT thread for publishing
		mqttc.publish(config['mqtt_publish_topic'], payload=json.dumps(payload))
		if runmode == 'debug':
			print("# MQTT: '%s'" % config['mqtt_publish_topic']+str(payload))
	payload.pop('ts')
	if config['mqtt_publish_format'] == 'plain' or config['mqtt_publish_format'] == 'both':
		topic = config['mqtt_publish_topic']
		camera = payload['camera']
		location = payload['location']
		sublocation = payload['sublocation']
		for key, value in payload.items():
			if key not in ['camera', 'location', 'sublocation']:
				mqttc.publish('%s/%s/%s/%s/%s' % (topic, location, sublocation, camera, key), value)
				if runmode == 'debug':
					print("# MQTT: '%s/%s/%s/%s/%s' " % (topic, location, sublocation, camera, key), value)

def influxdb_connect():
	from influxdb import InfluxDBClient
	global influxdbc
	influxdbc = InfluxDBClient(config['influxdb_hostname'], config['influxdb_port'], config['influxdb_username'], config['influxdb_password'], config['influxdb_dbname'])
	logger.warning("Creating InfluxDB client object.")

def publish_influxdb(payload):
	now = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
	fields = {}
	# Extract fields from payload
	for key, value in payload.items():
		if key == 'camera':
			camera = value
		elif key == 'sublocation':
			sublocation = value
		elif key == 'location':
			location = value
		elif key in ['cnts', 'classified', 'motion']:
			fields.update({key:float(value)})
	json_body = {
			"measurement": "xqtt-measurements",
			"tags": {
				"location": location,
				"sublocation": sublocation,
				"camera": camera
				},
	}
	json_body.update({'fields':fields, 'time':now})
	json_body = [json_body]
	if runmode == 'debug':
		print("# InfluxDB payload: '%s'" % json_body)
	if fields:
		influxdbc.write_points(json_body)

def webdavclient_connect():
	import webdav.client as wc
	from webdav.client import WebDavException
	global webdavclient
	global WebDavException
	options = {
	'webdav_hostname': "https://"+config['webdavclient_hostname']+"/"+config['webdavclient_folder'],
	'webdav_login':    config['webdavclient_username'],
	'webdav_password': config['webdavclient_password'],
	#'verbose'    : True
	}
	webdavclient = wc.Client(options)
	logger.warning("Creating Webdavclient client object.")
	
### This thread maintains envriomental variables and restarts program's threads in case they crash for any reason
class supervisor(threading.Thread):
	def __init__(self, name = None):
		threading.Thread.__init__(self, name = name)
		# Create buffers for subprocess
		mp.set_start_method('forkserver')
		self.influx = mp.Queue(1)
		self.efflux = mp.Queue()
		self. processes = []
		# Start detection MultiStick
		# Activation of inferencer
		if(classifier == 'tiny_yolo'):
			model_bin = config['tiny_yolo_classifier_model_bin']
			model_xml = config['tiny_yolo_classifier_model_xml']
		elif(classifier == 'yolo'):
			model_bin = config['yolo_classifier_model_bin']
			model_xml = config['yolo_classifier_model_xml']
		p = mp.Process(target=inferencer, name='inferencer', args=(self.efflux, self.influx, model_bin, model_xml, classifier), daemon=True)
		p.start()
		self.processes.append(p)
		logger.warning("(Re)-Starting inferencer subprocess...")
		return
	def run(self):
		while True:
			try:
				# Start MQTT thread if none running
				if "mqtt_thread" not in get_threads() and config['mqtt_broker_enabled']:
					thread = mqtt_connect(name = 'mqtt_thread')
					thread.daemon = True
					thread.start()
					logger.warning("(Re)-Starting mqtt_connect thread...")
				# # Classifier thread 
				if "ncsworker_bridge_thread" not in get_threads():
					if(classifier == 'tiny_yolo'):
						confidence = config['tiny_yolo_classifier_confidence']
					elif(classifier == 'yolo'):
						confidence = config['yolo_classifier_confidence']
					thread = ncsworker_bridge(name = "ncsworker_bridge_thread", efflux = self.efflux, influx = self.influx, confidence = confidence)
					thread.daemon = True
					thread.start()
					logger.warning("(Re)-Starting ncsworker_bridge thread...")
				# Start consumer threads if not already running, one per camera configured
				for camera, attributes in cameras.items():
					if cameras[camera]['classifier'] == classifier: 
						if str(camera)+"_camera_consumer_thread" not in get_threads() and cameras[camera]['enabled'] == 'True':
							thread_name = str(camera)+"_camera_consumer_thread"
							thread = camera_consumer(camera, thread_name)
							thread.daemon = True
							thread.start()
							logger.warning("(Re)-Starting camera_consumer thread for camera '%s' ..." % cameras[camera]['id'])
				if camera_class == 'outdoors': 
					# Publish thread 
					if "publish" not in get_threads():
						thread = publish(name = 'publish')
						thread.daemon = True
						thread.start()
						logger.warning("(Re)-Starting publish thread...")
					# Publish thread 
					if "save" not in get_threads():
						thread = save(name = 'save')
						thread.daemon = True
						thread.start()
						logger.warning("(Re)-Starting save thread...")
					# Consumer for motion detected frames
					if "motion_consumer_thread" not in get_threads():
						thread = motion_consumer(name = "motion_consumer_thread")
						thread.daemon = True
						thread.start()
						logger.warning("(Re)-Starting motion_consumer thread...")
				time.sleep(1)
			except Exception:
				sys.excepthook(*sys.exc_info())
			except:
				logger.exception("Other error or exception occurred!")
				if runmode == 'debug':
					print("Other error or exception occurred!")
			#finally:
			#	for p in range(len(self.processes)):
			#		self.processes[p].terminate()

class influx(threading.Thread):
	def __init__(self, topic = None, name = None, queueSize = 10):
		threading.Thread.__init__(self, name = name)
		self.topic = topic
		self.consumer = KafkaConsumer(self.topic , bootstrap_servers = config['kafka_broker_hostname']+":"+str(config['kafka_broker_port']))
		self.stopped = False
		self.count = 0
		self.skip = 10
		if(camera_class == 'indoors'):
			self.skip = 3
		# initialize the queue used to store frames read from
		# the video file
		self.Q = Queue(maxsize = queueSize)
	def start(self):
		# start a thread to read frames from the file video stream
		t = Thread(target=self.update, name=self.name, args=())
		t.daemon = True
		t.start()
		return self
	def update(self):
		# keep looping infinitely
		while True:
			# if the thread indicator variable is set, stop the
			# thread
			if self.stopped:
				return
			# otherwise, ensure the queue has room in it
			if not self.Q.full():
				for msg in self.consumer:
					self.count += 1
					if self.count == self.skip:
						self.Q.put(msg.value)
						self.count = 0
						#if runmode == 'debug':
						#	print("# Queue %s frame update!. Queued %s" % (self.topic, self.left()))

	def read(self):
		# return next frame in the queue
		return self.Q.get()
	def left(self):
		return self.Q.qsize()
	def more(self):
		# return True if there are still frames in the queue
		return self.Q.qsize() > 0
	def stop(self):
		# indicate that the thread should be stopped
		self.stopped = True

class camera_consumer(threading.Thread):
	def __init__(self, camera, name = None):
		threading.Thread.__init__(self, name = name)
		self.topic = cameras[camera]['topic']
		self.camera_classifier = cameras[camera]['classifier']
		self.camera_class = cameras[camera]['class']
		self.camera_location = cameras[camera]['location']
		self.camera_sublocation = cameras[camera]['sublocation']
		self.encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), int(cameras[camera]['quality'])]
		self.vs = influx(self.topic, self.name[0:16]+'_influx_thread').start()
		self.producer = KafkaProducer(bootstrap_servers = config['kafka_broker_hostname']+":"+str(config['kafka_broker_port']))
		self.frame = None
		### Initiate containers for MQTT payload here
		self.payload = {'camera': self.topic, 'camera_class':self.camera_class, 'camera_classifier':self.camera_classifier, 'location':self.camera_location, 'sublocation':self.camera_sublocation}
		return
	def run(self):
		while True:
			try:
				# grab frame
				frame = self.vs.read()
				# convert the frame to cv2 object
				nparr = np.fromstring(frame, np.uint8)
				self.frame = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
				jpeg = cv2.imencode('.jpg', self.frame)[1]
				base64_jpeg = base64.b64encode(jpeg).decode('utf-8')
				self.payload.update({'frame': base64_jpeg})
				# put the frame in the inbound queue with medium priority
				in_queue.put(2, self.payload)
			except Exception:
				if runmode == 'debug':
					print("# Camera Consumer: other error or exception occurred to the camera_consumer thread!")
				sys.excepthook(*sys.exc_info())
			except:
				logger.exception("Other error or exception occurred to the camera_consumer thread!")
				if runmode == 'debug':
					print("Other error or exception occurred to the camera_consumer thread!")

class motion_consumer(threading.Thread):
	def __init__(self, name = None):
		threading.Thread.__init__(self, name = name)
		self.topic = config['kafka_broker_subscribe']
		self.consumer = KafkaConsumer(self.topic , bootstrap_servers = config['kafka_broker_hostname']+":"+str(config['kafka_broker_port']))
		self.payload = None
		return
	def run(self):
		while True:
			try:
				for msg in self.consumer:
					# grab message
					self.payload = json.loads(msg.value.decode('utf-8'))
					if runmode == 'debug':
						print("# Motion Consumer: received motion triggered frame from %s!" %self.payload['deviceid'])
					# rename frame key to 'image'
					self.payload['frame'] = self.payload.pop('image')
					# add boolean key motion = true
					self.payload['motion'] = True
					# put the frame in inbound queue with highest priority 
					in_queue.put(1, self.payload)
			except Exception:
				if runmode == 'debug':
					print("# Motion Consumer: other error or exception occurred to the motion_consumer!")
				sys.excepthook(*sys.exc_info())
				break
			except:
				logger.exception("Other error or exception occurred to the motion_consumer!")
				if runmode == 'debug':
					print("Other error or exception occurred to the motion_consumer!")

### This thread handles delivery of mqtt and saving of images
class publish(threading.Thread):
	def __init__(self, name):
		threading.Thread.__init__(self, name = name)
		self.payload = None
		self.camera = None
		self.image = None
		self.producer = KafkaProducer(bootstrap_servers = config['kafka_broker_hostname']+":"+str(config['kafka_broker_port']))
		return
	def run(self):
		while True:
			try:
				# grab image from outbound queue
				self.payload = out_queue.get()
				self.camera = self.payload['camera']
				self.image = base64.b64decode(self.payload['frame'])
				
				# convert image to jpg 
				nparr = np.fromstring(self.image, np.uint8)
				self.image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
				jpeg = cv2.imencode('.jpg', self.image)[1]
				# send image
				self.producer.send(self.camera, jpeg.tobytes())
				if runmode == 'debug':
					print("# Publish: Kafka publish frame for %s completed!" % self.camera)
			except Exception:
				sys.excepthook(*sys.exc_info())
				logger.exception("Other error or exception occurred to publish!")
				if runmode == 'debug':
					print("Other error or exception occurred to publish!")
			except:
				logger.exception("Other error or exception occurred to publish!")
				if runmode == 'debug':
					print("Other error or exception occurred to publish!")

### This thread handles delivery of mqtt and saving of images
class save(threading.Thread):
	def __init__(self, name):
		threading.Thread.__init__(self, name = name)
		self.mobilenet_total_hit = 0
		self.mobilenet_total_miss = 0
		self.payload = None
		self.camera = None
		self.image = None
		return
	def run(self):
		while True:
			try:
				if runmode == 'debug':
					print("# Save: image save started")
				# grab enxt in queue
				self.payload = save_queue.get()
				# get time stamps
				self.day = datetime.now(pytz.timezone(config['timezone'])).strftime('%m-%d-%Y')
				self.now = datetime.now(pytz.timezone(config['timezone'])).strftime('%H:%M:%S')
				# grab the frame
				self.camera = self.payload['camera']
				self.classified = self.payload['classified']
				self.deviceid = self.payload['deviceid']
				# convert image 
				self.image = base64.b64decode(self.payload['frame'])
				nparr = np.fromstring(self.image, np.uint8)
				self.image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
				
				# Upload classified snapshot to webdav cloud storage
				if config['webdavclient_enabled'] and self.classified == 1:
					cv2.imwrite('/tmp/snapshot-mobilenet-'+self.now+'.jpg', self.image)
					file_name = 'snapshot-mobilenet-' + self.now + '.jpg'
					local_path = '/tmp/' + file_name
					remote_path = self.day + '/' + self.deviceid + '/'
					# Create remote folders
					if not webdavclient.check(self.day):
						webdavclient.mkdir(self.day)
					if not webdavclient.check(remote_path):
						webdavclient.mkdir(remote_path)
					# Upload snapshot
					if runmode == 'debug':
						print("# Save: uploading hit image %s to %s via webdav" % (file_name, remote_path))
					try:
						webdavclient.upload_sync(remote_path=remote_path+file_name, local_path=local_path)
					except WebDavException as exception:
						logger.exception("An error or exception occurred while uploading snapshot to webdav!")
					# Lastly remove local copy of classified snapshot
					os.remove(local_path)

				elif self.payload['motion'] and self.classified == 0:
					# create snapshot folders
					folder = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'snapshots/', self.deviceid, self.day)
					if not os.path.isdir(folder):
						os.makedirs(folder)
					cv2.imwrite(folder+'/snapshot-mobilenet-'+str(self.classified)+'-'+self.now+'.jpg', self.image)
					if runmode == 'debug':
						print("# Save: saving miss image locally")
				if runmode == 'debug':
					print("# Save: saving image finished for %s!" % self.deviceid)
			except Exception:
				sys.excepthook(*sys.exc_info())
				logger.exception("Other error or exception occurred to save thread!")
				if runmode == 'debug':
					print("# Save: Other error or exception occurred to save thread!")
			except:
				logger.exception("Other error or exception occurred to save thread!")
				if runmode == 'debug':
					print("# Save: Other error or exception occurred to save thread!")
					

##### TINY YOLO STUFF

def EntryIndex(side, lcoords, lclasses, location, entry):
	n = int(location / (side * side))
	loc = location % (side * side)
	return int(n * side * side * (lcoords + lclasses + 1) + entry * side * side + loc)

class DetectionObject():
	xmin = 0
	ymin = 0
	xmax = 0
	ymax = 0
	class_id = 0
	confidence = 0.0

	def __init__(self, x, y, h, w, class_id, confidence, h_scale, w_scale):
		self.xmin = int((x - w / 2) * w_scale)
		self.ymin = int((y - h / 2) * h_scale)
		self.xmax = int(self.xmin + w * w_scale)
		self.ymax = int(self.ymin + h * h_scale)
		self.class_id = class_id
		self.confidence = confidence

def IntersectionOverUnion(box_1, box_2):
	width_of_overlap_area = min(box_1.xmax, box_2.xmax) - max(box_1.xmin, box_2.xmin)
	height_of_overlap_area = min(box_1.ymax, box_2.ymax) - max(box_1.ymin, box_2.ymin)
	area_of_overlap = 0.0
	if (width_of_overlap_area < 0.0 or height_of_overlap_area < 0.0):
		area_of_overlap = 0.0
	else:
		area_of_overlap = width_of_overlap_area * height_of_overlap_area
	box_1_area = (box_1.ymax - box_1.ymin)  * (box_1.xmax - box_1.xmin)
	box_2_area = (box_2.ymax - box_2.ymin)  * (box_2.xmax - box_2.xmin)
	area_of_union = box_1_area + box_2_area - area_of_overlap
	retval = 0.0
	if area_of_union <= 0.0:
		retval = 0.0
	else:
		retval = (area_of_overlap / area_of_union)
	return retval

def ParseYOLOV3Output(blob, resized_im_h, resized_im_w, original_im_h, original_im_w, threshold, objects, classifier):

	if(classifier == 'yolo'):
		anchors = [10,13,16,30,33,23,30,61,62,45,59,119,116,90,156,198,373,326]
	elif(classifier == 'tiny_yolo'):
		anchors = [10,14, 23,27, 37,58, 81,82, 135,169, 344,319]
	classes = 80
	coords = 4
	num = 3
	yolo_scale_13 = 13
	yolo_scale_26 = 26
	yolo_scale_52 = 52
	out_blob_h = blob.shape[2]
	out_blob_w = blob.shape[3]
	side = out_blob_h
	anchor_offset = 0
	if len(anchors) == 18:   ## YoloV3
		if side == yolo_scale_13:
			anchor_offset = 2 * 6
		elif side == yolo_scale_26:
			anchor_offset = 2 * 3
		elif side == yolo_scale_52:
			anchor_offset = 2 * 0
	elif len(anchors) == 12: ## tiny-YoloV3
		if side == yolo_scale_13:
			anchor_offset = 2 * 3
		elif side == yolo_scale_26:
			anchor_offset = 2 * 0
	else:                    ## ???
		if side == yolo_scale_13:
			anchor_offset = 2 * 6
		elif side == yolo_scale_26:
			anchor_offset = 2 * 3
		elif side == yolo_scale_52:
			anchor_offset = 2 * 0

	side_square = side * side
	output_blob = blob.flatten()

	for i in range(side_square):
		row = int(i / side)
		col = int(i % side)
		for n in range(num):
			obj_index = EntryIndex(side, coords, classes, n * side * side + i, coords)
			box_index = EntryIndex(side, coords, classes, n * side * side + i, 0)
			scale = output_blob[obj_index]
			if (scale < threshold):
				continue
			x = (col + output_blob[box_index + 0 * side_square]) / side * resized_im_w
			y = (row + output_blob[box_index + 1 * side_square]) / side * resized_im_h
			height = math.exp(output_blob[box_index + 3 * side_square]) * anchors[anchor_offset + 2 * n + 1]
			width = math.exp(output_blob[box_index + 2 * side_square]) * anchors[anchor_offset + 2 * n]
			for j in range(classes):
				class_index = EntryIndex(side, coords, classes, n * side_square + i, coords + 1 + j)
				prob = scale * output_blob[class_index]
				if prob < threshold:
					continue
				obj = DetectionObject(x, y, height, width, j, prob, (original_im_h / resized_im_h), (original_im_w / resized_im_w))
				objects.append(obj)
	return objects

class ncsworker_bridge(threading.Thread):
	def __init__(self, name, efflux, influx, confidence):
		threading.Thread.__init__(self, name = name)
		self.efflux = efflux
		self.influx = influx
		self.confidence = confidence
		self.detected_confidence = 0
		self.fps = ""
		self.detectfps = ""
		self.framecount = 0
		self.detectframecount = 0
		self.time1 = 0
		self.time2 = 0
		self.lastresults = None

		self.label_text_color = (255, 255, 255)
		#self.label_text_color = (0, 34, 255)  # box color
		self.label_background_color = (125, 175, 75)
		#self.label_background_color = (0, 34, 255)
		self.box_color = (255, 128, 0)
		#self.box_color = (0, 34, 255)
		self.box_thickness = 3

		self.payload = {}
		self.mqtt_payload = {}
		self.camera = None
		self.image = None
		self.count = 0
		self.motion = 0
		self.cnts = 0
		
		self.labels = ("person", "bicycle", "car", "motorbike", "aeroplane", "bus", "train", "truck", "boat", "traffic light", "fire hydrant", "stop sign", "parking meter", "bench", "bird", "cat", "dog", "horse", "sheep", "cow", "elephant", "bear", "zebra", "giraffe", "backpack", "umbrella", "handbag", "tie", "suitcase", "frisbee", "skis", "snowboard", "sports ball", "kite", "baseball bat", "baseball glove", "skateboard", "surfboard","tennis racket", "bottle", "wine glass", "cup", "fork", "knife", "spoon", "bowl", "banana", "apple", "sandwich", "orange", "broccoli", "carrot", "hot dog", "pizza", "donut", "cake", "chair", "sofa", "pottedplant", "bed", "diningtable", "toilet", "tvmonitor", "laptop", "mouse", "remote", "keyboard", "cell phone", "microwave", "oven", "toaster", "sink", "refrigerator", "book", "clock", "vase", "scissors", "teddy bear", "hair drier", "toothbrush")

		if display:
			self.window_name = "Live Output"
			self.wait_key_time = 1
			cv2.namedWindow(self.window_name, cv2.WINDOW_AUTOSIZE)

	def run(self):
		while True:
			try:
				self.t1 = time.perf_counter()
				self.classified = 0
				self.presence = 0
				self.object = 0
				self.detected_confidence = 0
				self.asserted_confidence  = 0
				self.mqtt_publish = 0
				# grab image from inbound queue
				self.payload = in_queue.get()
				self.camera = self.payload['camera']
				self.camera_class = self.payload['camera_class']
				self.camera_location = self.payload['location']
				self.camera_sublocation = self.payload['sublocation']
				self.mqtt_payload.update({'camera': self.camera, 'location':self.camera_location, 'sublocation':self.camera_sublocation})
				self.payload.update({'classified': self.classified})
				
				if('motion' in self.payload):
					self.motion = self.payload['motion']
				else:
					self.motion = 0
				if('cnts' in self.payload):
					self.cnts = self.payload['cnts']
				else:
					self.cnts = 0
				# convert image to cv2 image
				frame = base64.b64decode(self.payload['frame'])
				nparr = np.fromstring(frame, np.uint8)
				self.image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
				
				# If frame full grab frame to release a place
				if not self.influx.full():
					#print("frame bufer full!")
					#self.influx.get()
					self.influx.put(self.image.copy())
				else:
					print("influx buffer full!")
					self.influx.get()

				if not self.efflux.empty():
					objects = self.efflux.get(False)
					#print(objects)
					self.detectframecount += 1
					
					#print(type(objects))
					#objlen = len(objects)
					#print("bridge sees %s objects" % objlen)

					for obj in objects:
						
						# If confidence too low skip the object
						if obj.confidence < self.confidence:
							#print("confidence way too low (seemed like a %s" % self.labels[label])
							continue
						if obj.confidence > self.confidence:
							# only process labels interested in
							#if(obj.class_id in [0, 1, 2, 3, 4, 5, 6, 7, 8, 15, 16, 17, 18, 19]):
							if(obj.class_id):
								# Flag frame as classified
								self.classified = 1
								# Grab label and confidence
								label = obj.class_id
								self.detected_confidence = obj.confidence
								
								self.object = self.labels[label]
								self.detected_confidence = round(obj.confidence * 100, 2)

								if runmode == 'debug':
									print("# ncsworker_bridge: classifier detected %s %s %s" %(self.camera, self.object, self.detected_confidence))

								# Flag presence if indoors camera running tiny yolo classifier
								if obj.class_id == 0 and self.camera_class == 'indoors':
									self.presence = 1
								else:
									self.presence = 0
								# Add labels and rectangles to outdoors camera then send back to kafka broker
								if(self.camera_class == 'outdoors'):
									label_text = self.labels[label] + " (" + "{:.1f}".format(self.detected_confidence) + "%)"
									cv2.rectangle(self.image, (obj.xmin, obj.ymin), (obj.xmax, obj.ymax), self.box_color, self.box_thickness)
									cv2.putText(self.image, label_text, (obj.xmin, obj.ymin - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.6, self.label_text_color, 1)
									# convert image
									jpeg = cv2.imencode('.jpg', self.image)[1]
									base64_jpeg = base64.b64encode(jpeg).decode('utf-8')
									self.payload.update({'classified': self.classified, 'frame': base64_jpeg})
									# put the classified frame in the outgoing kafka queue
									out_queue.put(self.payload)
									if runmode == 'debug':
										print("# ncsworker_bridge: classifier interval frame hit processing ended!")


								# MQTT processing for motion generated frames
								if self.motion == 1:
									print("hit 1st if")
									if self.classified == 0:
										print("hit 2nd if")
									if runmode == 'debug':
										print("# ncsworker_bridge: classifier motion miss processing ended!")
									# put the payload in save queue 
									save_queue.put(self.payload)
									# publish mqtt
									self.mqtt_payload.update({'motion': self.motion, 'classified':self.classified, 'cnts':self.cnts, 'object': self.object, 'confidence': self.detected_confidence})
									self.mqtt_publish = 1
									# publish influxdb
									if config['influxdb_enabled']:
										publish_influxdb(self.mqtt_payload)
								elif self.motion == 0:
									if self.camera_class == 'indoors' and self.classified == 0:
										print("hit 3rd if")
										self.mqtt_payload.update({'presence': self.presence})
										self.mqtt_publish = 1
										if runmode == 'debug':
											print("# ncsworker_bridge: classifier indoors miss processing ended!")
									elif self.camera_class == 'indoors' and self.classified == 1:
										print("hit 4th if")
										self.mqtt_payload.update({'presence': self.presence, 'object': self.object, 'confidence': self.detected_confidence})
										self.mqtt_publish = 1
										if runmode == 'debug':
											print("# ncsworker_bridge: classifier indoors hit processing ended!")
									elif self.camera_class == 'outdoors' and self.classified == 1:
										print("hit 5th if")
										self.mqtt_payload.update({'object': self.object, 'confidence': self.detected_confidence})
										self.mqtt_publish = 1
										if runmode == 'debug':
											print("# ncsworker_bridge: classifier outdoors hit processing ended!")
								# Publish mqtt
								if self.mqtt_publish:
									publish_mqtt(self.mqtt_payload)
								# clean up dict for next iteration
								self.mqtt_payload.pop('presence', None)
								self.mqtt_payload.pop('cnts', None)
								self.mqtt_payload.pop('motion', None)
								self.mqtt_payload.pop('object', None)
								self.mqtt_payload.pop('classified', None)
								self.mqtt_payload.pop('confidence', None)
					
				if display:
					height = self.image.shape[0]
					width = self.image.shape[1]
					cv2.imshow(self.window_name, cv2.resize(self.image, (width, height)))
					if cv2.waitKey(self.wait_key_time)&0xFF == ord('q'):
						sys.exit(0)

				## Print FPS
				self.framecount += 1
				#if self.framecount >= 5 and runmode == 'debug':
				if self.framecount >= 5:
					print("# ncsworker_bridge: playback FPS  %s" % round(self.time1/5, 2))
					print("# ncsworker_bridge: detection FPS %s" % round(self.detectframecount/self.time2, 2))
					#self.fps       = "(Playback) {:.1f} FPS".format(self.time1/15)
					#self.detectfps = "(Detection) {:.1f} FPS".format(self.detectframecount/self.time2)
					self.framecount = 0
					self.detectframecount = 0
					self.time1 = 0
					self.time2 = 0
				self.t2 = time.perf_counter()
				self.elapsedTime = self.t2-self.t1
				self.time1 += 1/self.elapsedTime
				self.time2 += self.elapsedTime

			except Exception:
				sys.excepthook(*sys.exc_info())
				logger.exception("Other error or exception occurred to ncsworker_bridge thread!")
				if runmode == 'debug':
					print("# Save: Other error or exception occurred to ncsworker_bridge thread!")
			except:
				logger.exception("Other error or exception occurred to ncsworker_bridge thread!")
				if runmode == 'debug':
					print("# Save: Other error or exception occurred to ncsworker_bridge thread!")
					

# l = Search list
# x = Search target value
def searchlist(l, x, notfoundvalue=-1):
	if x in l:
		return l.index(x)
	else:
		return notfoundvalue

def async_infer(ncsworker):
	#ncsworker.skip_frame_measurement()
	#ncsworker.skip_frame_measurement()
	while True:
		ncsworker.predict_async()
		time.sleep(0.01)

def inferencer(efflux, influx, model_bin, model_xml, classifier):
	# Launch the worker 
	thworker = threading.Thread(target=async_infer, args=(ncsworker(efflux, influx, model_xml, model_bin, classifier),))
	thworker.start()
	thworker.join()

class ncsworker(object):
	def __init__(self, efflux, influx, model_xml, model_bin, classifier):
		self.frameBuffer = influx
		self.results = efflux
		self.classifier = classifier
		
		self.model_xml = model_xml
		self.model_bin = model_bin

		self.m_input_size = 416
		self.threshold = 0.4
		#self.threshold = 0.7
		self.num_requests = 4
		self.inferred_request = [0] * self.num_requests
		self.heap_request = []
		self.inferred_cnt = 0
		self.plugin = IEPlugin(device="MYRIAD")
		self.net = IENetwork(model=self.model_xml, weights=self.model_bin)
		self.input_blob = next(iter(self.net.inputs))
		self.exec_net = self.plugin.load(network=self.net, num_requests=self.num_requests)
		
		self.predict_async_time = 800
		self.skip_frame = 0
		self.roop_frame = 0
		self.vidfps = 5
		self.image_width = 0
		self.image_height = 0
		self.new_w = 0
		self.new_h = 0

	def image_preprocessing(self, color_image):
		## Resize image
		self.image_width = color_image.shape[1]
		self.image_height = color_image.shape[0]
		self.new_w = int(self.image_width * min(self.m_input_size/self.image_width, self.m_input_size/self.image_height))
		self.new_h = int(self.image_height * min(self.m_input_size/self.image_width, self.m_input_size/self.image_height))
		resized_image = cv2.resize(color_image, (self.new_w, self.new_h), interpolation = cv2.INTER_CUBIC)
		canvas = np.full((self.m_input_size, self.m_input_size, 3), 128)
		canvas[(self.m_input_size-self.new_h)//2:(self.m_input_size-self.new_h)//2 + self.new_h,(self.m_input_size-self.new_w)//2:(self.m_input_size-self.new_w)//2 + self.new_w,  :] = resized_image
		prepimg = canvas
		prepimg = prepimg[np.newaxis, :, :, :]     # Batch size axis add
		prepimg = prepimg.transpose((0, 3, 1, 2))  # NHWC to NCHW
		return prepimg

	def skip_frame_measurement(self):
			surplustime_per_second = (1000 - self.predict_async_time)
			if surplustime_per_second > 0.0:
				frame_per_millisecond = (1000 / self.vidfps)
				total_skip_frame = surplustime_per_second / frame_per_millisecond
				self.skip_frame = int(total_skip_frame / self.num_requests)
			else:
				self.skip_frame = 0

	def predict_async(self):
		try:
			while True:
				if self.frameBuffer.empty():
					return

				self.roop_frame += 1
				if self.roop_frame <= self.skip_frame:
				   self.frameBuffer.get()
				   return
				self.roop_frame = 0

				prepimg = self.image_preprocessing(self.frameBuffer.get())
				reqnum = searchlist(self.inferred_request, 0)

				if reqnum > -1:
					self.exec_net.start_async(request_id=reqnum, inputs={self.input_blob: prepimg})
					self.inferred_request[reqnum] = 1
					self.inferred_cnt += 1
					if self.inferred_cnt == sys.maxsize:
						self.inferred_request = [0] * self.num_requests
						self.heap_request = []
						self.inferred_cnt = 0
					heapq.heappush(self.heap_request, (self.inferred_cnt, reqnum))

				cnt, dev = heapq.heappop(self.heap_request)

				if self.exec_net.requests[dev].wait(0) == 0:
					self.exec_net.requests[dev].wait(-1)

					objects = []
					outputs = self.exec_net.requests[dev].outputs
					for output in outputs.values():
						objects = ParseYOLOV3Output(output, self.new_h, self.new_w, self.image_height, self.image_width, self.threshold, objects, classifier)

					objlen = len(objects)
					print("ncs sees %s objects" % objlen)
					for i in range(objlen):
						if (objects[i].confidence == 0.0):
							continue
						for j in range(i + 1, objlen):
							if (IntersectionOverUnion(objects[i], objects[j]) >= 0.4):
								if(self.classifier == 'tiny_yolo'):
									if objects[i].confidence < objects[j].confidence:
										objects[i], objects[j] = objects[j], objects[i]
								objects[j].confidence = 0.0

					self.results.put(objects)
					self.inferred_request[dev] = 0
				else:
					heapq.heappush(self.heap_request, (cnt, dev))
		except:
			import traceback
			traceback.print_exc()

if __name__ == "__main__":
	print("opencv-openvino is spinning. Check /var/log/opencv-motion.log for more details...")
	# Load environment variables and config parameters from file config.cfg
	logger.warning("Loading configuration parameters from config file.")
	config_load()
	# Load cameras
	logger.warning("Loading camera config file.")
	camera_db_load()
	# Perfrom network checks and wait there until we can reach the MQTT broker
	logger.warning("Checking Network Servers are reachable.")
	check_network()
	if config['influxdb_enabled']:
		influxdb_connect()
	if config['webdavclient_enabled']:
		webdavclient_connect()
	try:
		# Supervisor thread starts last
		thread = supervisor(name = "supervisor_thread")
		thread.daemon = True
		thread.start()
		logger.warning("Started supervisor_thread thread...")
		if runmode == 'debug':
			while True:
				print("# Number of active threads: %s" % threading.active_count())
				print(get_threads())
				time.sleep(30.0 - ((time.time() - starttime) % 30.0))
	except KeyboardInterrupt:
		logger.exception("Got exception on main handler:")
		if runmode == 'debug':
			print("Got exception on main handler:")
		raise
	except:
		logger.exception("Other error or exception occurred!")
		if runmode == 'debug':
			print("Other error or exception occurred!")
	while True:
		time.sleep(0.1)
		pass
