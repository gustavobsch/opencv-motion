#!/usr/bin/python3
import configparser, os, datetime, time, logs, socket, threading, sys, cv2
from queue import Queue
from threading import Thread
import paho.mqtt.client as mqtt
from kafka import KafkaProducer
from datetime import datetime
from utils import utils as utils
import numpy as np
import base64
import json
import pytz

logger = logs.setup_logger('opencv-motion')
logger.warning("Program Started...")
starttime = time.time()

### Get run mode
if len(sys.argv) > 1:
	runmode = sys.argv[1]
elif len(sys.argv) == 1:
	runmode = 'normal'

def config_load():
	global config
	global sun
	global wind
	global single_camera
	wind = 0
	sun = "above_horizon"
	config = {}
	config_file = configparser.RawConfigParser()
	config_file.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), '', 'config.cfg'))
	config.update({
	'hostid':utils.chop_comment(config_file.get('host', 'id')),
	'host_mac':utils.chop_comment(config_file.get('host', 'mac')),
	'timezone':config_file.get('environment', 'timezone'),
	'multicam_enabled':config_file.getboolean('influx', 'multicam'),
	'mqtt_broker_enabled':config_file.getboolean('mqtt_broker', 'enabled'),
	'mqtt_broker_hostname':utils.chop_comment(config_file.get('mqtt_broker', 'hostname')),
	'mqtt_broker_port':int(utils.chop_comment(config_file.get('mqtt_broker', 'port'))),
	'mqtt_broker_username':utils.chop_comment(config_file.get('mqtt_broker', 'username')),
	'mqtt_broker_pass':utils.chop_comment(config_file.get('mqtt_broker', 'password')),
	'mqtt_publish_topic':utils.chop_comment(config_file.get('mqtt_broker', 'publish_topic')),
	'mqtt_subscribe_wind_topic':utils.chop_comment(config_file.get('mqtt_broker', 'wind_topic')),
	'mqtt_subscribe_sun_topic':utils.chop_comment(config_file.get('mqtt_broker', 'sun_topic')),
	'mqtt_publish_format':utils.chop_comment(config_file.get('mqtt_broker', 'format')),
	'kafka_broker_hostname':utils.chop_comment(config_file.get('kafka_broker', 'hostname')),
	'kafka_broker_port':int(utils.chop_comment(config_file.get('kafka_broker', 'port'))),
	'kafka_broker_compression':utils.chop_comment(config_file.get('kafka_broker', 'compression')),
	'kafka_broker_publish':utils.chop_comment(config_file.get('kafka_broker', 'publish')),
	})
	if not config['multicam_enabled']:
		single_camera = os.environ['CAMERA']
		config['hostid'] = config['hostid']+"_"+single_camera

def camera_db_load():
	global cameras
	global single_camera
	cameras = {}
	# Load camera_db config file
	camera_db = configparser.RawConfigParser()
	camera_db.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), '', 'camera_db.cfg'))
	if config['multicam_enabled']:
		# Fill camera_db dictionary
		for camera in camera_db.sections():
			cameras[camera] = {}
			for key, value in camera_db.items(camera):
				cameras[camera][key] = value
	else:
		# Fill camera_db dictionary
		for camera in camera_db.sections():
			for key, value in camera_db.items(camera):
				if key == 'id' and value == single_camera:
					cameras[camera] = {}
					for ckey, cvalue in camera_db.items(camera):
						cameras[camera][ckey] = cvalue

def check_network():
	mqtt = 0
	kafka = 0
	while mqtt == 0:
		try:
			s = socket.create_connection((socket.gethostbyname(config['mqtt_broker_hostname']), config['mqtt_broker_port']), 2)
			logger.warning("MQTT broker network check passed!, '%s' is reachable." % config['mqtt_broker_hostname'])
			mqtt = 1
		except Exception:
			logger.warning("MQTT broker network check failed!, stalling until it becomes reachable...")
			time.sleep(15)
	while kafka == 0:
		try:
			s = socket.create_connection((socket.gethostbyname(config['kafka_broker_hostname']), config['kafka_broker_port']), 2)
			logger.warning("Kafka network check passed!, '%s' is reachable." % config['kafka_broker_hostname'])
			kafka = 1
		except Exception:
			logger.warning("Kafka network check failed!, stalling until it becomes reachable...")
			time.sleep(15)


### MQTT funcionts here
def on_connect(client, userdata, flags, rc):
	# Subscribing in on_connect() means that if we lose the connection and
	# reconnect then subscriptions will be renewed.
	if rc == 0:
		logger.warning("Succesfully connected to MQTT broker, starting subscriptions if any.")
		# Start Subscriptions here
		client.subscribe(config['mqtt_subscribe_wind_topic'], 1)
		logger.warning("Subscribed to topic '%s'" % config['mqtt_subscribe_wind_topic'])
		client.subscribe(config['mqtt_subscribe_sun_topic'], 1)
		logger.warning("Subscribed to topic '%s'" % config['mqtt_subscribe_sun_topic'])
	else:
		logger.warning("Connection to MQTT broker failed. rc='%s' " % rc)

def on_publish(client, userdata, mid):
	logger.debug("PUBLISH '%s' completed." % mid)

def on_message(client, userdata, msg):
	global wind
	global sun
	if msg.topic == config['mqtt_subscribe_wind_topic']:
		wind = float(msg.payload)
		if runmode == 'debug':
			print("Refreshing wind speed %s" % wind)
	elif msg.topic == config['mqtt_subscribe_sun_topic']:
		sun = str(msg.payload, 'utf-8')
		if runmode == 'debug':
			print("Refreshing sun %s" % sun)

def on_disconnect(client, userdata, rc):
	if rc != 0:
		logger.warning("Unexpected MQTT Broker disconnection. Will auto-reconnect.")

def on_log(mosq, obj, level, string):
	logger.debug(string)

### Classes / functions start here
class mqtt_connect(threading.Thread):
	def __init__(self, name = None):
		threading.Thread.__init__(self, name = name)
		return
	def run(self):
		global mqttc
		mqttc = mqtt.Client(config['hostid'])
		mqttc.username_pw_set(config['hostid'], config['hostid'])
		mqttc.connect(config['mqtt_broker_hostname'], config['mqtt_broker_port'], 80)
		mqttc.on_connect = on_connect
		mqttc.on_message = on_message
		mqttc.on_publish = on_publish
		mqttc.on_disconnect = on_disconnect
		# Uncomment here to enable MQTT debug messages
		#mqttc.on_log = on_log
		while True:
			mqttc.loop()

### This thread maintains envriomental variables and restarts program's threads in case they crash for any reason
class supervisor(threading.Thread):
	def __init__(self, name = None):
		threading.Thread.__init__(self, name = name)
		return
	def run(self):
		while True:
			try:
				# Start MQTT thread if none running
				if "mqtt_thread" not in get_threads() and config['mqtt_broker_enabled']:
					thread = mqtt_connect(name = 'mqtt_thread')
					thread.daemon = True
					thread.start()
					logger.warning("Restarting mqtt_connect thread...")
				for camera, attributes in cameras.items():
					if str(camera)+"_influx_thread" not in get_threads() and cameras[camera]['enabled'] == 'True' and cameras[camera]['class'] == 'indoors':
						thread_name = str(camera)+"_influx_thread"
						thread = influx(camera, thread_name)
						thread.daemon = True
						thread.start()
						logger.warning("Restarted dead influx thread for camera '%s' ..." % cameras[camera]['id'])
				time.sleep(1)
			except Exception:
				sys.excepthook(*sys.exc_info())
			except:
				logger.exception("Other error or exception occurred!")
				if runmode == 'debug':
					print("Other error or exception occurred!")

def get_threads():
	threads = []
	main_thread = threading.current_thread()
	for t in threading.enumerate():
		if t is main_thread:
			continue
			#print(t.getName())
		threads.append(t.getName())
	return threads

class stream:
	def __init__(self, camera = None, name = None, queueSize = 32):
		# initialize the file video stream along with the boolean
		# used to indicate if the thread should be stopped or not
		self.name = name
		self.stream = cv2.VideoCapture(camera)
		self.stopped = False
		self.Q = Queue(maxsize = queueSize)
	def start(self):
		# start a thread to read frames from the file video stream
		t = Thread(target=self.update, name=self.name, args=())
		t.daemon = True
		t.start()
		return self
	def update(self):
		# keep looping infinitely
		while True:
			# if the thread indicator variable is set, stop the
			# thread
			if self.stopped:
				return
			# otherwise, ensure the queue has room in it
			if not self.Q.full():
				(grabbed, frame) = self.stream.read()
				self.Q.put(frame)
	def read(self):
		# return next frame in the queue
		return self.Q.get()
	def more(self):
		# return True if there are still frames in the queue
		return self.Q.qsize() > 0
	def stop(self):
		# indicate that the thread should be stopped
		self.stopped = True

class influx(threading.Thread):
	def __init__(self, camera, name = None):
		threading.Thread.__init__(self, name = name)
		self.deviceid = cameras[camera]['id']
		self.camera = self.deviceid
		self.location = cameras[camera]['location']
		self.sublocation = cameras[camera]['sublocation']
		self.camera_class = cameras[camera]['class']
		self.topic = cameras[camera]['topic']
		self.encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), int(cameras[camera]['quality'])]
		self.vs = stream(cameras[camera]['url'], self.name[0:16]+'_stream_thread').start()
		self.producer = KafkaProducer(bootstrap_servers = config['kafka_broker_hostname']+":"+str(config['kafka_broker_port']))
		self.payload = {'camera':self.camera, 'camera_class':self.camera_class, 'deviceid':self.deviceid, 'location':self.location, 'sublocation':self.sublocation}
		self.frame = None
		return
	def run(self):
		while True:
			try:
				# grab frame
				lag = time.time()
				ts = datetime.now(pytz.timezone(config['timezone'])).strftime('%Y-%m-%d %H:%M:%S.%f')
				self.frame = self.vs.read()
				# skip frame processing when frame is black during night time
				if sun == 'below_horizon':
					if np.sum(self.frame) < 2000000:
						continue
				# convert frame to string
				self.frame = base64.b64encode(cv2.imencode('.jpg', self.frame, self.encode_param)[1]).decode('utf-8')
				# add frame and time stamp do payload 
				self.payload.update({'frame':self.frame, 'ts': ts, 'lag':lag})
				# send the payload
				self.producer.send(self.topic, json.dumps(self.payload).encode('utf-8'))
			except Exception:
				sys.excepthook(*sys.exc_info())
				self.producer.flush()
				self.producer.close()
				self.vs.stop()
				if runmode == 'debug':
					print("Other error or exception occurred!")
				break
			except:
				self.producer.flush()
				self.producer.close()
				self.vs.stop()
				logger.exception("Other error or exception occurred!")
				if runmode == 'debug':
					print("Other error or exception occurred!")

if __name__ == "__main__":
	print("opencv-influx is spinning. Check /var/log/opencv-motion.log for more details...")
	# Load environment variables and config parameters from file config.cfg
	logger.warning("Loading configuration parameters from config file.")
	config_load()
	# Perfrom network checks and wait there until we can reach the MQTT broker
	logger.warning("Checking Kafka Broker is reachable.")
	check_network()
	# Load cameras
	logger.warning("Loading camera config file.")
	camera_db_load()
	try:
		# Supervisor thread starts last
		thread = supervisor(name = 'supervisor')
		thread.daemon = True
		thread.start()
		logger.warning("Started supervisor thread...")
		if runmode == 'debug':
			while True:
				print("Number of active threads: %s" % threading.active_count())
				print(get_threads())
				time.sleep(30.0 - ((time.time() - starttime) % 30.0))
	except KeyboardInterrupt:
		logger.exception("Got exception on main handler:")
		if runmode == 'debug':
			print("Got exception on main handler:")
		mqttc.loop_stop()
		mqttc.disconnect()
		raise

	except:
		logger.exception("Other error or exception occurred!")
		if runmode == 'debug':
			print("Other error or exception occurred!")
		mqttc.loop_stop()
		mqttc.disconnect()

	while True:
		time.sleep(0.1)
		pass
