from kafka import SimpleProducer, KafkaClient
from threading import Thread
from multiprocessing import Queue
import time
import socket
import argparse
import threading
import sys
import logs
import cv2

logger = logs.setup_logger('opencv-motion')
logger.warning("Program Started...")
starttime = time.time()

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-b", "--broker", default='kafka.noip.us:9093',
	help="Kafka Broker")
ap.add_argument("-t", "--topic", default='raw-'+socket.gethostname(),
	help="Kafka Topic")
ap.add_argument("-w", "--width", type=int, default=1024,
	help="Capture Webcam Width")
ap.add_argument("-he", "--height", type=int, default=768,
	help="Capture Webcam Height")
ap.add_argument("-f", "--framerate", type=int, default=5,
	help="Capture Webcam Framerate")
ap.add_argument("-q", "--quality", type=int, default=85,
	help="Capture Webcam Quality")
ap.add_argument("-d", "--debug", type=bool, default=0,
	help="Debug Enable")
ap.add_argument("-v", "--video",
	help="Video to play")
args = vars(ap.parse_args())

if(args["debug"]) == 1:
	runmode = 'debug'
else:
	runmode = 'normal'

def get_threads():
	threads = []
	main_thread = threading.current_thread()
	for t in threading.enumerate():
		if t is main_thread:
			continue
			#print(t.getName())
		threads.append(t.getName())
	return threads

### This thread maintains envriomental variables and restarts program's threads in case they crash for any reason
class supervisor(threading.Thread):
	def __init__(self, name = None):
		threading.Thread.__init__(self, name = name)
		return
	def run(self):
		while True:
			try:
				# Start MQTT thread if none running
				if "video_emitter" not in get_threads():
					thread = video_emitter(name = 'video_emitter')
					thread.daemon = True
					thread.start()
					logger.warning("Restarting video_emitter thread...")
				time.sleep(1)

			except Exception:
				sys.excepthook(*sys.exc_info())

			except:
				logger.exception("Other error or exception occurred!")
				if runmode == 'debug':
					print("Other error or exception occurred!")

class stream(threading.Thread):
	def __init__(self, name = None, queueSize = 32):
		threading.Thread.__init__(self, name = name)
		self.vs = cv2.VideoCapture(args["video"])
		self.stopped = False
		# initialize the queue used to store frames read from
		# the video file
		self.Q = Queue(maxsize = queueSize)
	def start(self):
		# start a thread to read frames from the file video stream
		t = Thread(target=self.update, name=self.name, args=())
		t.daemon = True
		t.start()
		return self
	def update(self):
		# keep looping infinitely
		while True:
			# if the thread indicator variable is set, stop the
			# thread
			if self.stopped:
				return
			# otherwise, ensure the queue has room in it
			if not self.Q.full():
				(grabbed, frame) = self.vs.read()
				self.Q.put(frame)
	def read(self):
		# return next frame in the queue
		return self.Q.get()
	def more(self):
		# return True if there are still frames in the queue
		return self.Q.qsize() > 0
	def stop(self):
		# indicate that the thread should be stopped
		self.stopped = True

class video_emitter(threading.Thread):
	def __init__(self, name = None):
		threading.Thread.__init__(self, name = name)
		self.topic = args["topic"]
		self.broker = args["broker"]
		self.kafka = KafkaClient(self.broker)
		self.producer = SimpleProducer(self.kafka)
		self.stream = stream(self.name[0:16]+'_stream_thread').start()
		self.starttime = time.time()
		self.count = 0
	def run(self):
		try:
			while True:
				self.count += 1
				if self.count == 6:
					print("FPS: %s" % round((self.count / ((time.time() - self.starttime))), 2))
					self.starttime = time.time()
					self.count = 0
				frame = self.stream.read()
				jpeg = cv2.imencode('.jpg', self.stream.read(), [int(cv2.IMWRITE_JPEG_QUALITY), 50])[1]
				self.producer.send_messages(self.topic, jpeg.tobytes())
				#time.sleep(0.1)

		except Exception:
			if runmode == 'debug':
				print("Other error or exception occurred to the video_emitter thread!")
			sys.excepthook(*sys.exc_info())

		except:
			logger.exception("Other error or exception occurred to the video_emitter thread!")
			if runmode == 'debug':
				print("Other error or exception occurred to the video_emitter thread!")

if __name__ == '__main__':
	print("opencv-producer is spinning. Check /var/log/opencv-motion.log for more details...")
	try:
		# Supervisor thread starts last
		thread = supervisor(name = "supervisor_thread")
		thread.daemon = True
		thread.start()
		logger.warning("Started supervisor_thread thread...")
		if runmode == 'debug':
			while True:
				print("Number of active threads: %s" % threading.active_count())
				print(get_threads())
				time.sleep(30.0 - ((time.time() - starttime) % 30.0))

	except KeyboardInterrupt:
		logger.exception("Got exception on main handler:")
		if runmode == 'debug':
			print("Got exception on main handler:")
		raise

	except:
		logger.exception("Other error or exception occurred!")
		if runmode == 'debug':
			print("Other error or exception occurred!")

	while True:
		time.sleep(0.1)
		pass
